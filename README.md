# singreg

Computation, validation, and benchmark of
singular and regular coefficients in cracked media.


## bibliography


@article{leguillon2008crack,
	Author = {Leguillon, Dominique and Murer, S{\'e}bastien},
	Journal = {International Journal of Fracture},
	Number = {1},
	Pages = {75},
	Title = {Crack deflection in a biaxial stress state},
	Volume = {150},
	Year = {2008}}

@inbook{hutchinson1991mixed,
	Author = {Hutchinson, J. W. and Suo, Z. and Hutchinson, John W. and Wu, Theodore Y.},
	Pages = {63--191},
	Publisher = {Elsevier},
	Title = {Mixed Mode Cracking in Layered Materials},
	Volume = {29},
	Year = {1991}}

@article{yang1999evaluation,
	Author = {Yang, B. and Ravi-Chandar, K.},
	Journal = {Engineering Fracture Mechanics},
	Number = {5},
	Pages = {589--605},
	Title = {Evaluation of elastic T-stress by the stress difference method},
	Volume = {64},
	Year = {1999}}

@article{kfouri1986some,
	Author = {Kfouri, A. P.},
	Journal = {International Journal of Fracture},
	Number = {4},
	Pages = {301--315},
	Title = {Some evaluations of the elastic T-term using Eshelby's method},
	Volume = {30},
	Year = {1986}}

@article{henry1994three-dimensional,
	Author = {Henry, B. S. and Luxmoore, A. R.},
	Journal = {International Journal of Fracture},
	Number = {1},
	Pages = {35--50},
	Title = {Three-dimensional evaluation of the T-stress in centre cracked plates},
	Volume = {70},
	Year = {1994}}

@article{sladek1997evaluation,
	Author = {Sladek, J. and Sladek, V.},
	Journal = {International Journal of Fracture},
	Number = {3},
	Pages = {199--219},
	Title = {Evaluation of T-stresses and stress intensity factors in stationary thermoelasticity by the coservation integral method},
	Volume = {86},
	Year = {1997}}

@article{gupta2015a-review,
	Author = {Gupta, M. and Alderliesten, R. C. and Benedictus, R.},
	Journal = {Engineering Fracture Mechanics},
	Pages = {218--241},
	Title = {A review of T-stress and its effects in fracture mechanics},
	Volume = {134},
	Year = {2015}}


FEniCSx
- Documentation: https://docs.fenicsproject.org/
- Tutorial: https://jorgensd.github.io/dolfinx-tutorial/
- Info: https://fenics2021.com/info.html
- Workflow: https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow
